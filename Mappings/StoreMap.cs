﻿using FluentNHibernate.Mapping;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mappings
{
    public class StoreMap : ClassMap<Store>
    {
        public StoreMap()
        {
            Id(s => s.Id);
            Map(s => s.StoreName, "Name");
            Map(s => s.StreetAddress);
            Map(s => s.PhoneNumber);
            References<ZipCode>(s => s.ZipCode, "ZipCode").Cascade.All();
            HasMany<Video>(s => s.Videos).Cascade.All();
            
        }
    }
}
