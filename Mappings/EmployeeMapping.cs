﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using FluentNHibernate.Mapping;

namespace Mappings
{
    public class EmployeeMapping : ClassMap<Employee>
    {
        public EmployeeMapping()
        {
            Id(e => e.Id);
            Map(e => e.DateHired);
            Map(e => e.DateOfBirth);
            Component(c => c.Name, m => {
                m.Map(n => n.Title);
                m.Map(n => n.First);
                m.Map(n => n.Middle);
                m.Map(n => n.Last);
                m.Map(n => n.Suffix);
            });
            Map(e => e.Password);
            Map(e => e.Username);
            References<Store>(e => e.Store, "Store").Cascade.All();
            References<Employee>(e => e.Supervisor, "Supervisor").Cascade.All();
        }
    }
}
