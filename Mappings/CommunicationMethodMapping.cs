﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using FluentNHibernate.Mapping;

namespace Mappings
{
    public class CommunicationMethodMapping : ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMapping()
        {
            Id(c => c.Id);
            Map(c => c.Frequency);
            Map(c => c.Units);
            Map(c => c.Name);
            HasManyToMany<Customer>(c => c.Customers).Table("CustomerCommunicationMethod").Inverse().Cascade.All();
        }
    }
}
