﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using FluentNHibernate.Mapping;

namespace Mappings
{
    public class RatingMapping : ClassMap<Rating>
    {
        public RatingMapping()
        {
            Id(r => r.Id);
            Map(r => r.Comment);
            Map(r => r.Score);
            References<Rental>(r => r.Rental, "Rental").Unique().Cascade.All();
        }
    }
}
