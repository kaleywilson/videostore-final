﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using FluentNHibernate.Mapping;

namespace Mappings
{
    public class RentalMapping : ClassMap<Rental>
    {
        public RentalMapping()
        {
            Id(r => r.Id);
            Map(r => r.DueDate);
            Map(r => r.RentalDate);
            Map(r => r.ReturnDate);
            References<Customer>(r => r.Customer, "Customer").Cascade.All();
            References<Video>(r => r.Video, "Video").Cascade.All();
            HasOne(r => r.Rating).PropertyRef(x => x.Rental).Cascade.All();
        }
    }
}
