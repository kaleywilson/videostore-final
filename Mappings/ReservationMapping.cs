﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using FluentNHibernate.Mapping;

namespace Mappings
{
    public class ReservationMapping : ClassMap<Reservation>
    {
        public ReservationMapping()
        {
            Id(r => r.Id);
            Map(r => r.ReservationDate);
            References<Movie>(r => r.Movie, "Movie").Cascade.All();
            References<Customer>(r => r.Customer, "Customer").Unique().Cascade.All();
            
        }
    }
}
