﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
        private int superId;

        [SetUp]
        public void CreateSession()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();

            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rating").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Rental").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.CommunicationMethod").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Reservation").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Employee").ExecuteUpdate();


            var sql = "insert into videostore.ZipCode(City, Code, State) values('holland', '81734', 'Michigan') " +
                "insert into videostore.Store(Name, PhoneNumber, StreetAddress, Zipcode) " +
                "values('Holland Store', '6416296015', '123 E 12th St', (select Code from videostore.ZipCode where Code = '81734')) " +
                "insert into videostore.employee(DateHired, DateOfBirth, First, Last, TopLevel, Supervisor, Password, Username, Store) " +
                "values(DATETIMEFROMPARTS(2021, 4, 22, 2, 23, 23, 3), DATETIMEFROMPARTS(2000, 4, 22, 23, 23, 34, 23), 'Kaley', 'Wilson', 1, null, " +
                "'password', 'kaley.wilson', (select Id from videostore.Store where Zipcode = '81734')) ";

            var query = _session.CreateSQLQuery(sql);

            query.ExecuteUpdate();

            sql = "select Id from videostore.Employee where First = 'Kaley'";

            query = _session.CreateSQLQuery(sql);

            superId = (int)query.UniqueResult();

            starWars = _session.Load<Movie>("tt0076759 ");
            var title = starWars.Title;

            hoosiers = _session.Load<Movie>("tt0091217 ");
            title = hoosiers.Title;
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan");
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                new ZipCode {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                },
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
           
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            var commtypes = new HashSet<CommunicationMethod>
            {
                new CommunicationMethod
                {
                    Frequency = 10,
                    Name = "email",
                    Units = CommunicationMethod.TimeUnit.Day
                },
                new CommunicationMethod
                {
                    Frequency = 13,
                    Name = "phone",
                    Units = CommunicationMethod.TimeUnit.Week
                }
            };

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };

            new PersistenceSpecification<Customer>(_session)
                .CheckProperty(c => c.Name, new Name()
                {
                    Title = "Dr.",
                    First = "Ryan",
                    Middle = "L",
                    Last = "McFall",
                    Suffix = "Sr"
                })
                .CheckProperty(c => c.EmailAddress, "mcfall@hope.edu")
                .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
                .CheckProperty(c => c.Password, "Abc123$!")
                .CheckProperty(c => c.Phone, "616-395-7952")
                .CheckReference(c => c.ZipCode,
                    new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList(
                    c => c.PreferredStores, stores,
                    (cust, store) => cust.AddPreferredStore(store)
                )
                .CheckInverseBag(c => c.CommunicationTypes, commtypes)
                .VerifyTheMappings();
        }

        [Test]
        public void CustomerToReservation()
        {
            var cust = new Customer()
            {
                Name = new Name()
                {
                    First = "John",
                    Last = "James",
                },
                EmailAddress = "john.james@gmail.com",
                Password = "Aand@bandc1",
                Phone = "6165406542",
                StreetAddress = "167 E 15th Street",
                ZipCode = new ZipCode()
                {
                    Code = "50309",
                    City = "Des Moines",
                    State = "Iowa"
                }
            };

            _session.Save(cust);

            var cust_id = cust.Id;

            var res = new Reservation()
            {
                Customer = cust,
                Movie = starWars,
                ReservationDate = new DateTime(2000, 4, 22)
            };

            _session.Save(res);
            _session.Evict(cust);

            cust = _session.Get<Customer>(cust_id);

            Assert.AreEqual(starWars.TitleId, res.Movie.TitleId);
            Assert.AreEqual(new DateTime(2000, 4, 22), res.ReservationDate);
            Assert.AreEqual(cust_id, res.Customer.Id);
        }

        [Test]
        public void CustomerToRentalsMappingIsCorrect()
        {
            var cust = new Customer()
            {
                Name = new Name()
                {
                    First = "John",
                    Last = "James",
                },
                EmailAddress = "john.james@gmail.com",
                Password = "Alldone3!",
                Phone = "6165406542",
                StreetAddress = "167 E 15th Street",
                ZipCode = new ZipCode()
                {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                }
            };

            _session.Save(cust);

            var cust_id = cust.Id;

            var rental1 = new Rental()
            {
                RentalDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(7),
                ReturnDate = DateTime.Now.AddDays(3),
                Customer = cust,
                Video = new Video()
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "82732",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                }
            };
            var rental2 = new Rental()
            {
                RentalDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(7),
                ReturnDate = DateTime.Now.AddDays(2),
                Customer = cust,
                Video = new Video ()
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "First Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "98776",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                }
            };

            _session.Save(rental1);
            _session.Save(rental2);

            _session.Evict(cust);

            cust = _session.Get<Customer>(cust_id);

            Assert.AreEqual(2, cust.Rentals.Count());
            Assert.AreEqual("Last Store", cust.Rentals[0].Video.Store.StoreName);
            Assert.AreEqual("First Store", cust.Rentals[1].Video.Store.StoreName);

        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };

            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, 
                    new ZipCode() {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }

        [Test]
        public void MovieMappingIsCorrect()
        {            
            Assert.AreEqual("tt0076759 ", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
            Assert.AreEqual("Action", starWars.PrimaryGenre.Name);

            //testing the Reservations list mapping to movie
            var cust = new Customer()
            {
                Name = new Name()
                {
                    First = "John",
                    Last = "James",
                },
                EmailAddress = "john.james@gmail.com",
                Password = "Aand@bandc1",
                Phone = "6165406542",
                StreetAddress = "167 E 15th Street",
                ZipCode = new ZipCode()
                {
                    Code = "84729",
                    City = "Holland",
                    State = "Michigan"
                }
            };

            var res1 = new Reservation() {
                Movie = starWars,
                ReservationDate = new DateTime(2000, 2, 22),
                Customer = cust
            };

            var res2 = new Reservation()
            {
                Movie = starWars,
                ReservationDate = new DateTime(2000, 2, 23),
                Customer = cust
            };

            _session.Save(res1);
            _session.Save(res2);

            _session.Evict(starWars);

            starWars = _session.Get<Movie>("tt0076759 ");

            Assert.AreEqual(2, starWars.Reservations.Count());
            Assert.AreEqual(new DateTime(2000, 2, 22), starWars.Reservations[0].ReservationDate);
            Assert.AreEqual(new DateTime(2000, 2, 23), starWars.Reservations[1].ReservationDate);

        }

        [Test]
        public void CommunicationMappingIsCorrect()
        {
            new PersistenceSpecification<CommunicationMethod>(_session)
                .CheckProperty(c => c.Frequency, 10)
                .CheckProperty(c => c.Name, "Email")
                .CheckProperty(c => c.Units, CommunicationMethod.TimeUnit.Day)
                .VerifyTheMappings();
        }
        

        [Test]
        public void EmployeeMappingIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            //get supervisor
            var super = _session.Get<Employee>(superId);
            //check if supervisor exists
            Assert.AreEqual("Kaley", super.Name.First); 
            
            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                .CheckProperty(e => e.DateHired, DateTime.Now)
                .CheckProperty(e => e.DateOfBirth, new DateTime(2000, 12, 22))
                .CheckProperty(e => e.Name, new Name()
                {
                    First = "John",
                    Last = "James",
                })
                .CheckReference(e => e.Supervisor, super)
                .CheckProperty(e => e.Password, "kaleyIsAwesome")
                .CheckProperty(e => e.Username, "JohnJames")
                .CheckReference(e => e.Store, new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                })
                .VerifyTheMappings();

        }


        [Test]
        public void RentalMappingIsCorrect()
        {

            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.RentalDate, DateTime.Now)
                .CheckProperty(r => r.DueDate, DateTime.Now.AddDays(7))
                .CheckProperty(r => r.ReturnDate, DateTime.Now.AddDays(3))
                .CheckReference(r => r.Customer, new Customer()
                {
                    Name = new Name()
                    {
                        First = "John",
                        Last = "James",
                    },
                    EmailAddress = "john.james@gmail.com",
                    Password = "Aand@bandc1",
                    Phone = "6165406542",
                    StreetAddress = "167 E 15th Street",
                    ZipCode = new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }   
                })
                .CheckReference(r => r.Video, new Video ()
                { 
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "82732",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                })
                .VerifyTheMappings();  
        }

        [Test]
        public void RatingMappingIsCorrect()
        {
            
            var cust = new Customer()
            {
                Name = new Name()
                {
                    First = "John",
                    Last = "James",
                },
                EmailAddress = "john.james@gmail.com",
                Password = "Aand@bandc1",
                Phone = "6165406542",
                StreetAddress = "167 E 15th Street",
                ZipCode = new ZipCode()
                {
                    Code = "84729",
                    City = "Holland",
                    State = "Michigan"
                }
            };
            
            var rental = new Rental()
            {
                RentalDate = DateTime.Now,
                DueDate = DateTime.Now.AddDays(7),
                ReturnDate = DateTime.Now.AddDays(2),
                Customer = cust,
                Video = new Video()
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "First Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "98776",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                },
                
            };

            
            _session.Save(rental);
            var rental_id = rental.Id;

            var rating = new Rating
            {
                Comment = "rating is good",
                Score = 3,
                Rental = rental
            };
            _session.Save(rating);
            _session.Evict(rental);

            rental = _session.Get<Rental>(rental_id);

            Assert.AreEqual(3, rental.Rating.Score);
            Assert.AreEqual("rating is good", rental.Rating.Comment);
            Assert.AreEqual(rental_id , rental.Rating.Rental.Id);

        }
        [Test]
        public void ReservationMappingIsCorrect()
        {
            var customer = new Customer()
            {
                Name = new Name()
                {
                    First = "John",
                    Last = "James",
                },
                EmailAddress = "john.james@gmail.com",
                Password = "Aand@bandc1",
                Phone = "6165406542",
                StreetAddress = "167 E 15th Street",
                ZipCode = new ZipCode()
                {
                    Code = "84729",
                    City = "Holland",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Reservation>(_session, new DateEqualityComparer())
                .CheckProperty(r => r.ReservationDate, DateTime.Now)
                .CheckReference(r => r.Movie, starWars)
                .CheckReference(r => r.Customer, customer)
                .VerifyTheMappings();
        }
    }
}
