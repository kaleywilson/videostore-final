﻿insert into videostore.ZipCode(City, Code, State)
values('holland', '81734', 'Michigan')

insert into videostore.Store(Name, PhoneNumber, StreetAddress, Zipcode)
values('Holland Store', '6416296015', '123 E 12th St', (select Code from videostore.ZipCode where Code = '81734'))


insert into videostore.employee(DateHired, DateOfBirth, First, Last, TopLevel, Supervisor, Password, Username, Store)
values(DATETIMEFROMPARTS(2021, 4, 22, 2, 23, 23, 3), DATETIMEFROMPARTS(2000, 4, 22, 23, 23, 34, 23), 'Kaley', 'Wilson', 1, null, 'password', 'kaley.wilson', (select Id from videostore.Store where Zipcode = '81734'))

insert into videostore.employee(DateHired, DateOfBirth, First, Last, TopLevel, Supervisor, Password, Username, Store)
values(DATETIMEFROMPARTS(2021, 4, 22, 2, 23, 23, 3), DATETIMEFROMPARTS(2000, 4, 22, 23, 23, 34, 23), 'Jeff', 'Williams', 0, (select Id from videostore.employee where First = 'Kaley'), 'password', 'kaley.wilson', (select Id from videostore.Store where Zipcode = '81734'))