﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(23)]
    public class AddUniqueConstraintForCustomerStoreOrderToPrefers : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("CustomerStoreOrderUnique")
                .FromTable("Prefers")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.UniqueConstraint("CustomerStoreOrderUnique")
                .OnTable("Prefers")
                .WithSchema("videostore")
                .Columns("Customer_Id", "PrefersOrder");
        }
    }
}
