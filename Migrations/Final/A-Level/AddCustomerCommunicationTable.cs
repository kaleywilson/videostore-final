﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(34)]
    public class AddCustomerCommunicationTable : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ToCommunicationMethod")
                .OnTable("CustomerCommunicationMethod")
                .InSchema("videostore");

            Delete.ForeignKey("ToCustomer")
                .OnTable("CustomerCommunicationMethod")
                .InSchema("videostore");

            Delete.Table("CustomerCommunicationMethod")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CustomerCommunicationMethod")
                .InSchema("videostore")
                .WithColumn("Customer_Id").AsInt32().PrimaryKey()
                .WithColumn("CommunicationMethod_Id").AsInt32().PrimaryKey();

            Create.ForeignKey("ToCustomer")
                .FromTable("CustomerCommunicationMethod")
                .InSchema("videostore")
                .ForeignColumn("Customer_Id")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);

            Create.ForeignKey("ToCommunicationMethod")
                .FromTable("CustomerCommunicationMethod")
                .InSchema("videostore")
                .ForeignColumn("CommunicationMethod_Id")
                .ToTable("CommunicationMethod")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
