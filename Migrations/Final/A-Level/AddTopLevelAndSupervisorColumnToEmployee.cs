﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(36)]
    public class AddTopLevelAndSupervisorColumnToEmployee : Migration
    {
        public override void Down()
        {
        
            Delete.Column("Supervisor")
                .FromTable("Employee")
                .InSchema("videostore");

            Delete.Column("TopLevel")
                .FromTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee")
                .InSchema("videostore")
                .AddColumn("TopLevel").AsBoolean().WithDefaultValue(0)
                .AddColumn("Supervisor").AsInt32().Nullable();
            
        }
    }
}
