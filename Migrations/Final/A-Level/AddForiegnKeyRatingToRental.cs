﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(35)]
    public class AddForiegnKeyRatingToRental : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RatingToRental")
                 .OnTable("Rating")
                 .InSchema("videostore");

            Delete.Column("Rental")
                .FromTable("Rating")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rating")
                 .InSchema("videostore")
                 .AddColumn("Rental").AsInt32().NotNullable();

            Create.ForeignKey("RatingToRental")
                .FromTable("Rating")
                .InSchema("videostore")
                .ForeignColumn("Rental")
                .ToTable("Rental")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.None);
            
        }
    }
}
