﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(37)]
    public class AddConstraintToEmployee : Migration
    {
        public override void Down()
        {
            Execute.Sql(
               "alter table videostore.employee drop constraint ifTopLevelTrueSupervisorNull"
               );
        }

        public override void Up()
        {
            Execute.Sql(
                    "alter table videostore.employee add constraint ifTopLevelTrueSupervisorNull " +
                    "check((TopLevel = 1 and Supervisor is null) or (TopLevel = 0 and Supervisor is not null))"
                    );
        }
    }
}
