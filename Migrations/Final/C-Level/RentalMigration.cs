﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(27)]
    public class RentalMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("Rental").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Rental").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("DueDate").AsDateTime().NotNullable()
                .WithColumn("RentalDate").AsDateTime().NotNullable()
                .WithColumn("ReturnDate").AsDateTime().Nullable();
        }
    }
}
