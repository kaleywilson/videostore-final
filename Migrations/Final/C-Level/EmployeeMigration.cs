﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(26)]
    public class EmployeeMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("Employee").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Employee").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                //.WithColumn("TopLevel").AsBoolean().NotNullable()
                .WithColumn("DateHired").AsDateTime().NotNullable()
                .WithColumn("DateOfBirth").AsDateTime().NotNullable()
                .WithColumn("IsManager").AsBoolean().Nullable()
                .WithColumn("First").AsString().NotNullable()
                .WithColumn("Middle").AsString().Nullable()
                .WithColumn("Last").AsString().NotNullable()
                .WithColumn("Title").AsString().Nullable()
                .WithColumn("Suffix").AsString().Nullable()
                .WithColumn("Password").AsString().NotNullable()
                .WithColumn("Username").AsString().NotNullable() ;
        }
    }
}
