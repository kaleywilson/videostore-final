﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(30)]
    public class AddForiegnKeyEmployeeToStore : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("EmployeeToStore")
                .OnTable("Employee")
                .InSchema("videostore");

            Delete.Column("Store")
                .FromTable("Employee")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee")
                .InSchema("videostore")
                .AddColumn("Store").AsInt32().NotNullable();

            Create.ForeignKey("EmployeeToStore")
                .FromTable("Employee")
                .InSchema("videostore")
                .ForeignColumn("Store")
                .ToTable("Store")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
