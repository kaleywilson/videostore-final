﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(29)]
    public class AddForiegnKeyRentalToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RentalToCustomer")
                .OnTable("Rental")
                .InSchema("videostore");

            Delete.Column("Customer")
                .FromTable("Rental")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Customer").AsInt32().NotNullable();

            Create.ForeignKey("RentalToCustomer")
               .FromTable("Rental")
               .InSchema("videostore")
               .ForeignColumn("Customer")
               .ToTable("Customer")
               .InSchema("videostore")
               .PrimaryColumn("Id")
               .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
