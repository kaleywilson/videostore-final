﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(31)]
    public class AddForeignKeyReservationToMovie : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ReservationToMovie")
                .OnTable("Reservation")
                .InSchema("videostore");

            Delete.Column("Movie")
                .FromTable("Reservation")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Reservation")
                .InSchema("videostore")
                .AddColumn("Movie").AsCustom("char(10)").NotNullable();

            Create.ForeignKey("ReservationToMovie")
                .FromTable("Reservation")
                .InSchema("videostore")
                .ForeignColumn("Movie")
                .ToTable("Movies")
                .InSchema("imdb")
                .PrimaryColumn("TitleID")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
