﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(33)]
    public class AddForeignKeyReservationToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ReservationToCustomer")
                .OnTable("Reservation")
                .InSchema("videostore");

            Delete.Column("Customer")
                .FromTable("Reservation")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Reservation")
                .InSchema("videostore")
                .AddColumn("Customer"). AsInt32().NotNullable();

            Create.ForeignKey("ReservationToCustomer")
                .FromTable("Reservation")
                .InSchema("videostore")
                .ForeignColumn("Customer")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
