﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(32)]
    public class AddForeignKeyRentalToVideo : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("RentalToVideo")
                .OnTable("Rental")
                .InSchema("videostore");

            Delete.Column("Video")
                .FromTable("Rental")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Video").AsInt32().NotNullable();

            Create.ForeignKey("RentalToVideo")
                .FromTable("Rental")
                .InSchema("videostore")
                .ForeignColumn("Video")
                .ToTable("Video")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.None);
        }
    }
}
